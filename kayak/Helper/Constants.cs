﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace kayak.Helper
{
    public class Constants
    {
        #region Error Constants
        public const string errorEmptyFrom = "Пожалуйста, выберите место получения.";
        public const string errorEmptyTo = "Пожалуйста, выберите место возврата.";
        public const string errorEndDateLessThanStartDate = "Дата возврата должна быть позже даты получения. Пожалуйста, поменяйте даты и/или время.";

        #endregion

        public const string baseurl = "https://www.kayak.ru/";

        public static IWebDriver Driver
        {
            get
            {
                //ChromeOptions ops = new ChromeOptions();
                //ops.AddArguments("--disable-notifications");
                return new ChromeDriver();
            }
        }

       

        #region Int Constants
        public const int Timeout1 = 20;
        public const int Timeout2 = 10;
        public const int TimeoutAfterClick = 1000;
        public const int TimeoutAssert = 500;
        public const int TimeoutForTimeSpan = 30;
        public const int TimeoutSwitchToCurrentWindowHandle = 4000;
        public const int LongTimeout = 3000;
        public const int ShortTimeout = 500;
        #endregion

    }
}
